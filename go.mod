module mongo-term

go 1.15

require (
	github.com/rivo/tview v0.0.0-20210217110421-8a8f78a6dd01
	go.mongodb.org/mongo-driver v1.4.6
)
