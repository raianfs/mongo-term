package main

import (
	"fmt"
	"sort"

	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	app             *tview.Application
	collectionsList *tview.List
	databaseList    *tview.List
	tableData       *tview.Table
	flex            *tview.Flex
	pages           *tview.Pages
)

func getSize(size int) string {
	unit := "b"
	s := float64(size)
	if s > 1024 {
		s = s / 1024
		unit = "Kb"
	}

	if s > 1024 {
		s = s / 1024
		unit = "Mb"
	}

	if s > 1024 {
		s = s / 1024
		unit = "Gb"
	}

	if s > 1024 {
		s = s / 1024
		unit = "Tb"
	}

	return fmt.Sprintf("%.2f%s", s, unit)
}

func fillDatabaseList(onSelectDatabase func(string)) error {
	l, err := mongoClient.ListDatabases(nil, bson.M{})
	if err != nil {
		return err
	}

	for _, db := range l.Databases {
		var m map[string]interface{}
		err := mongoClient.Database(db.Name).RunCommand(nil, bson.M{"dbStats": 1}).Decode(&m)
		if err != nil {
			fmt.Println(err)
		}
		size := getSize(int(m["dataSize"].(float64)))
		dbName := db.Name
		databaseList.AddItem(dbName, size, 0, func() { onSelectDatabase(dbName) })
	}

	return nil
}

func getCollections(db string) ([]map[string]interface{}, error) {
	collections, err := mongoClient.Database(db).ListCollections(nil, bson.M{})
	if err != nil {
		return nil, err
	}

	var m []map[string]interface{}
	err = collections.All(nil, &m)
	if err != nil {
		return nil, err
	}

	return m, nil
}

func getCollectionSize(dbName, collName string) int {
	var m map[string]interface{}
	err := mongoClient.Database(dbName).RunCommand(nil, bson.M{"collStats": collName}).Decode(&m)
	if err != nil {
		return 0
	}

	return int(m["size"].(int32))
}

func fillCollectionList(db string) {
	collections, _ := getCollections(db)
	var collectionsNames []string
	for _, coll := range collections {
		collectionsNames = append(collectionsNames, coll["name"].(string))
	}

	collectionsList.Clear()
	sort.Strings(collectionsNames)

	t := 0
	for _, collName := range collectionsNames {
		c := collName
		size := getCollectionSize(db, c)
		t += size
		collectionsList.AddItem(c, getSize(size), 0, func() { fillDataTable(db, c) })
	}
	collectionsList.SetBorder(true).SetTitle("Collections")
	app.SetFocus(collectionsList)
}

func fillDataTable(db, collection string) {

	//_, _, _, height := tableData.GetRect()

	coll := mongoClient.Database(db).Collection(collection)

	opt := options.Find()
	//opt.SetLimit(int64(height-32))
	cur, err := coll.Find(nil, bson.M{}, opt)
	if err != nil {
		fmt.Println(err)
		return
	}

	var m []map[string]interface{}
	err = cur.All(nil, &m)
	if err != nil {
		fmt.Println(err)
		return
	}

	tableData.SetBorders(true)
	tableData.Clear()
	tableData.SetInputCapture(captureKey)

	if len(m) == 0 {
		return
	}

	var headers []string

	for key := range m[0] {
		headers = append(headers, key)
	}

	sort.Strings(headers)
	for i, h := range headers {
		cell := tview.NewTableCell(h)
		cell.SetTextColor(tcell.ColorDarkRed)
		tableData.SetCell(0, i, cell)
	}

	for row, v := range m {
		for column, h := range headers {
			cell := tview.NewTableCell(fmt.Sprintf("%v", v[h]))
			tableData.SetCell(row+1, column, cell)
		}
	}

	tableData.SetFixed(1, 0)

	pages.SwitchToPage("data")
	app.SetFocus(tableData)
}

func captureKey(event *tcell.EventKey) *tcell.EventKey {
	if event.Key() == tcell.KeyEsc {
		switch app.GetFocus() {
		case collectionsList:
			app.SetFocus(databaseList)
		case tableData:
			pages.SwitchToPage("main")
			app.SetFocus(collectionsList)
		}

		return nil
	}

	if event.Key() == tcell.KeyTAB {
		switch app.GetFocus() {
		case databaseList:
			app.SetFocus(collectionsList)
		case collectionsList:
			pages.SwitchToPage("data")
			app.SetFocus(tableData)
		}

		return nil
	}

	return event
}

func init() {
	app = tview.NewApplication()

	databaseList = tview.NewList().SetHighlightFullLine(true)
	databaseList.SetBorderPadding(1, 1, 1, 1).
		SetInputCapture(captureKey).
		SetTitle("Databases").
		SetBorder(true)

	collectionsList = tview.NewList().SetHighlightFullLine(true)
	collectionsList.SetBorderPadding(1, 1, 1, 1).
		SetInputCapture(captureKey).
		SetTitle("Collections").
		SetBorder(true)

	tableData = tview.NewTable()
	tableData.SetBorder(true).SetTitle("Data")

	flex = tview.NewFlex().
		AddItem(databaseList, 0, 1, true).
		AddItem(collectionsList, 0, 1, false)

	pages = tview.NewPages().
		AddPage("main", flex, true, true).
		AddPage("data", tableData, true, false)
}

func main() {
	err := fillDatabaseList(fillCollectionList)
	if err != nil {
		panic(err)
	}

	if err := app.SetRoot(pages, true).SetFocus(flex).Run(); err != nil {
		panic(err)
	}
}
